/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.cvm;



import accessoracledb.CVMBundle;
import accessoracledb.DBSrverClient;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import javax.xml.soap.SOAPMessage;
import mtngc.sms.SMSClient;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import org.apache.commons.lang.StringUtils;
/**
 *
 *  @author JcMoutoh
 */
public class USSDHandler  {
    
   static String ContextKeyName;
    static ArrayList <Integer> supportedMSISDNs;
    String FreeFlow;
    String CustomerMsisdn;
    String responseCode= null;
    String header = "Bienvenu sur MTN CVM";
    String additionalMsg ;
    boolean allowed;
    double balance = 0;
    int sc = 0;
    private static int[] supportedServiceClasses = {1, 2, 3, 4, 6, 15, 47, 38, 12, 39, 31, 23, 36, 80,90};
    private static int[] validServiceClasses = {40,28, 20,7,100, 19,25,88,41};
    //boolean isCalledMsisdn = IsCalledMsisdn(CalledMsisdnFirst,CalledMsisdnSecond);
    static
      {  
        ContextKeyName = "allcvm";
        if (System.getProperty("os.name").toLowerCase().contains("windows")){
        String filePath = "C:\\Logs\\"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
        }
        else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
       String filePath = "/Users/macbook/logs/ussd_"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
            
            
        }
        else  {
        String filePath = "/var/log/ussd_"+ContextKeyName+"_v1";
          try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
            MLogger.setHomeDirectory(filePath);
        }
        
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
            
        try {
            String msisdnStr = request.getParameter("MSISDN");
            //msisdnStr = "224660134178";
            int msisdn = 0;
            try{msisdnStr = msisdnStr.substring(3);}catch(Exception n){};
            try{msisdn = Integer.parseInt(msisdnStr);}catch(Exception n){};
            
            if(IsMSISDNAllowed(msisdn)){
                processRequest(msisdnStr,request, response);
            }else {
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "jcmk2544");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");           
                out.close();            
            }
            
        } catch (Exception e) {
            MLogger.Log(this, e);
        }    
    }
    
    public void processRequest(String msisdn, HttpServletRequest request, HttpServletResponse response){
//        String FreeFlow = "FC";
            this.FreeFlow="FC";
            
        try {
            
            String ussdSessionid = request.getParameter("SESSIONID");            
            String input = request.getParameter("INPUT");
            
            //String msg = null;
   
            ServletContext context = request.getServletContext();
            
                StringBuilder sb = new StringBuilder();
             
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing request.");
                            
                String contextKey = GetContextKey(ussdSessionid);
                Object obj = context.getAttribute(contextKey);
                if(obj == null){
                    if(input != null){
                        String str = displayFirstMenu(context, ussdSessionid, msisdn);
                        sb.append(str );
                    }else
                        sb.append("Application is running");
                }else {
                    
                    BundleSession bundleSession = (BundleSession) obj;
                    bundleSession.setFreeFlow("FC");
                    int step = bundleSession.getStep();
                    String ussdCode = bundleSession.getUssdCode();
                    
                   
                    // if we display the Menu
                    
                    if (step == 1){
                         String str = processMainMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                         sb.append(str);
                    }
                   
                  
                }
                
            //}
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", this.FreeFlow);  
            response.setHeader("cpRefId", "jcmk2545");
            //response.setContentType(type);
            PrintWriter out = response.getWriter();
//            out.append("Your string goes here\n");
//            out.append("Your string goes here");
            
            out.append(sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }  
    
    // Process Main Input
        
    public String processMainMenuInput(String input, String msisdn,String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        String str = null;
         if((input != null) && ( !input.trim().equals(""))  && (StringUtils.isNumeric(input.trim())) ){
            input =  input.trim();
            StringBuilder sb = new StringBuilder();
            RequestEnum reqEnum = null;
          
            if(input.equals("1")){
                
                reqEnum = RequestEnum.ACTIVER;
                DBSrverClient cvmData = new DBSrverClient();
                List<CVMBundle> list = cvmData.getCVMPackages(msisdn);
                int i=1;
                for(CVMBundle  cvm : list){
                   String RefillId =cvm.getRefillId();
                   ProvisioningEngine engin = new ProvisioningEngine();
                    msg = engin.executeRefillIdRequest(msisdn, ussdSessionid, cvm, RefillId);
                    str = "Votre requete est en cours de traitement. Vous recevrez un message dans un instant";
                    this.FreeFlow = "FB";
                    this.Cleanup(context, ussdSessionid);
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+ussdSessionid+"|"+RefillId+"|Message:"+msg);
                     i++;
                     return str;
                }
                
      
            }
           
            else{
              reqEnum = RequestEnum.ANNULER;
              str =  "Cher client, Tapez *100*3# pour acheter un nouveau pass";  
                    this.FreeFlow = "FB";
                    this.Cleanup(context, ussdSessionid);
             return str;
            
            } 
//                            
        }
        else{
//            naughty user, he entered wrong information
            msg = displayFirstMenu(context, ussdSessionid, msisdn);
//msg =  "Cher client, Tapez *100*3# pour acheter un nouveau pass";  
        }    
        return msg;
    }

    // Process First Menu  
    
    protected String displayFirstMenu(ServletContext context, String ussdSessionid, String msisdn){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list");
        BundleSession bundleSession = new BundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        MainMenu mainMenu = new MainMenu(msisdn,ussdSessionid,false);
        String str = mainMenu.getString();
        if(!mainMenu.isAllowed()){
            this.FreeFlow = "FC";
        }
        
        return str;
    }
    protected void TraceParametersAndHeaders(HttpServletRequest request){
        
        Enumeration<String> headerEnum = request.getHeaderNames();
        MLogger.Log(this, LogLevel.DEBUG, "Tracing HTTP Headers and Parameters ");
        
        while(headerEnum.hasMoreElements()) {
            String headerName = headerEnum.nextElement();
            String headerValue = request.getHeader(headerName);
            //System.out.println("Header:- "+headerName+": "+headerValue);
            MLogger.Log(this, LogLevel.DEBUG, "Header:- "+headerName+": "+headerValue);
        } 
        
        Map<String, String[]> map = request.getParameterMap();
        //Reading the Map
        //Works for GET && POST Method
        for(String paramName:map.keySet()) {
            String[] paramValues = map.get(paramName);

            //Get Values of Param Name
            for(String valueOfParam:paramValues) {
                //Output the Values
                //System.out.println("Value of Param with Name "+paramName+": "+valueOfParam);
                //ystem.out.println("Parameter:- "+paramName+": "+valueOfParam);
                MLogger.Log(this, LogLevel.DEBUG, "Parameter:- "+paramName+": "+valueOfParam);
            }
        }           
        
    }
    
    private void SuccessNotifcation(String CustomerMsisdn){
         StringBuilder sb = new StringBuilder();
//        msisdnCstr = "224" + msisdnCstr;// "224664222412";
        if (CustomerMsisdn !=null){
          String msg = "Solde:*223#ok; Recharge carte:*224*code#ok; Mobile Money:*440#ok; Services MTN:*100#ok; Autres Services:*444#ok; Support Client:111; Message WhatsApp:660222222";  
          SMSClient smsClient = new SMSClient();
          smsClient.sendSMS("BIENVENUE CHEZ MTN", CustomerMsisdn, msg);
        }
    }
    
    protected void Cleanup(ServletContext context, String ussdSessionid){
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
    }
    protected String GetContextKey(String ussdSessionid){
        if (ContextKeyName == null)
            throw new RuntimeException("ContextKeyName not set");
        return ContextKeyName+"-"+ussdSessionid;
    } 
    
    protected boolean IsMSISDNAllowed(int msisdn){
        if((supportedMSISDNs == null) || (supportedMSISDNs.size() == 0))
            return true;
        if(msisdn == 0)
            return true;
        for(int n : supportedMSISDNs){
            if(n==msisdn)
                return true;
        }
        
        return false;
    }
    
    
    public String NotAllowed(){
        StringBuilder sb = new StringBuilder();
        //sb.append(header+"\n");
        
        sb.append("Vous n etes pas autorise a utiliser ce service. Pour plus d informations, appelez le 111.\n");
        if(additionalMsg != null)
            sb.append(additionalMsg);
              
        String str = sb.toString();
        return str;
    }
    
//    
      
    public boolean isAllowed(){
        return this.allowed;
    }
    
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
   private boolean IsServiceClassValid(int sc){
        for(int n : validServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
   
 
    private boolean isMsisdn(String input){
        boolean ok = false;
        if((input != null) && ( !input.trim().equals(""))){
            input = input.trim();
            //664222545
            if(input.length() == 9){
                if(StringUtils.isNumeric(input) ){
                    ok = true;
                }
            }
        }
        return ok;
    }
    
  
    
}
