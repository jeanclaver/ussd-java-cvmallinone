/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.cvm;
import mtngc.ussd.AbstractBundleSession;
import accessoracledb.CVMBundle;
import accessoracledb.DBSrverClient;
/**
 *
 * @author JcMoutoh
 */
public class BundleSession  extends AbstractBundleSession {
    private String ussdSessionId;
    private String msisdn;
    private CVMBundle selectedSegment;
    private String ussdCode;
    private String freeFlow;
    private int step;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the selectedSegment
     */
    public CVMBundle getSelectedSegment() {
        return selectedSegment;
    }

    /**
     * @param selectedSegment the selectedSegment to set
     */
    public void setSelectedSegment(CVMBundle selectedSegment) {
        this.selectedSegment = selectedSegment;
    }

    /**
     * @return the ussdCode
     */
    public String getUssdCode() {
        return ussdCode;
    }

    /**
     * @param ussdCode the ussdCode to set
     */
    public void setUssdCode(String ussdCode) {
        this.ussdCode = ussdCode;
    }

    public String getFreeFlow() {
        return freeFlow;
    }

    public void setFreeFlow(String freeFlow) {
        this.freeFlow = freeFlow;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
   
    
}
