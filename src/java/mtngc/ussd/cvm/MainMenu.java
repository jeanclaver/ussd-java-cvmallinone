/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.cvm;


import accessoracledb.CVMBundle;
import accessoracledb.DBSrverClient;
import java.util.*;
//import dbaccess.dbsrver.datareseller.*;

import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
//import mtngc.ussd.AbstractMainMenu;
//import mtngc.bundles.core.*;
/**
 *
* @author JcMoutoh
 */
public final class MainMenu  {
String header;
String msg;
String additionalMsg ;
boolean allowed;
String FreeFlow;
String msisdn; 

public MainMenu(String msisdnStr,String transactionId,boolean confirm)  {
       Init ( msisdnStr,transactionId,confirm);
    }

    MainMenu(CVMBundle msisdnStr, String TransactionId, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected void Init(String msisdnStr,String transactionId,boolean confirm) {
        header = "Special pour vous";//SWAPP
        
        try{
            if (!confirm)
            {
                

                msisdn = msisdnStr;
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Fetching Subscriber Service Class details");
//                ResellerProvisioningEngine engine = new ResellerProvisioningEngine(); 
//                int sc = engine.getServiceClass(msisdnStr, transactionId);
                boolean isPos = IsPos(msisdn);
                
                
                
                
//                if ( ((sc != 31) && (sc != 40 )) && (!isBrandSupport) ) {
                if ( !isPos ) {
                    MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdnStr+"| is not configured to use this service ");
                   
                    msg=NotAllowed();
                     this.FreeFlow = "FC";
                    
                }else
                {
                    if(isPos){
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdnStr+" is configured to use this SIM SWAPP service");
                    }
                    allowed = true;
                    msg=Process();
                    this.FreeFlow = "FC";
                }
            } else{
                msg= ProcessConfirm();
            }
        }catch (Exception e) {
               MLogger.Log(this, e);
        }    
    }
    
   private boolean IsPos(String msisdn){
         boolean resp = false;
       DBSrverClient dbClient = new DBSrverClient();
        CVMBundle dbBs = dbClient.getEligibleCVM(msisdn);
        if(dbBs != null)
            resp = true;
        return resp;
    }
    
    public String NotAllowed(){
        
        StringBuilder sb = new StringBuilder();
        sb.append(header).append("\n");
        sb.append("Cher client, vous n etes pas eligible pour cette promo. Tapez *440*3*1# et gagnez jusqu a 50% de Bonnus sur MTN MoMo");
        if(additionalMsg != null)
            sb.append(additionalMsg);
        this.FreeFlow = "FC";      
        String str = sb.toString();
        return str;
    }
    
    
    
    public String getString(){
        return msg;
    }
    
   public String Process(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        sb.append("\n");
        DBSrverClient dbClient = new DBSrverClient();
        List<CVMBundle> list = dbClient.getCVMPackages(msisdn);
        
       int i=1;
        for(CVMBundle  cvm : list){
          sb.append(""+cvm.getConfirmationTxt() +"\n");

            
            i++;
        }
        sb.append("\n");
        sb.append("1-Confirmer \n");
       
        sb.append("\n");
        sb.append("Repondez");
        return  sb.toString();
        
    }

   public String ProcessConfirm(){
        StringBuilder sb = new StringBuilder();
        //sb.append(header+"\n");
        sb.append("Veuillez confirmer le numero du client\n"); 
        sb.append("\n");
        sb.append("Repondez");
        return  sb.toString();
        
    }
    
    public boolean isAllowed(){
        return this.allowed;
    }
    
    
}
